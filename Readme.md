# LoRa Serial

Library to send and receive LoRa packets using a modem connected via a serial port. I wanted to be able to send and receive LoRa communications with my Raspberry Pi. This library allows me to connect a Moteino M0 via USB and use it for that purpose.

I built this library for use in production for some of my clients, so it shoudl be quite reliable. That said, I was in a hurry so an elegant API design was not the highest priority. Pull requests to improve that are welcome!

## Usage Example

```rust
use lora_serial::{LoRaSerial};

fn main() {
    let lora = LoRaSerial.init_all().first().expect("Failed to connect to any LoRa modem.");

    // send a packet to  the device at address 2
    lora.tx(2, "Hello, World");

    // print anything we receive
    loop {
        if let Some(packet) = lora.rx().expect("Error trying to receive a packet") {
            println!("{:?}", packet);
        }
    }
}
```

## Testing

Tests for this library require a pair of Moteino M0's programmed with a compliant firmware. You can find the firmware I used as a platformio project in the modem_firmware directory. Tests must not run in parallel or there will be contention for the serial port.

```sh
cargo test -- --test-threads=1
```
